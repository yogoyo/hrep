# Version 0.4

## Version 0.4.1a

* Fix int parsing to accept hex literals
* Fix nibble patterns not working correctly
* Allow using '.' in hex pattern instead of '?'

## Version 0.4.0a

* Dropped support for Python 2.x

### New features
* Can supply line count to `-A`, `-B` and `-C` arguments, e.g `-C 3L` will print 3 lines above and below each match.
* Search for numbers with `-n <number>[:format]`. The optional `format` argument is a `struct` format string. 
By default the integer is encoded as little-endian and padded with zeros to the nearest unsigned 8-bit length.
E.g: 
    - `-n 55` will search for `\x37`
    - `-n 256` will search for `\x00\x01`
    - `-n 1337:>L` will search for `\x39\x05\x00\x00`
